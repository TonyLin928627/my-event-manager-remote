'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');


const SERVICE_ACCOUNT = './serviceAccount.json';

const serviceAccount = require(SERVICE_ACCOUNT);
const dataBaseUrl = 'https://mydemos-fc5a9.firebaseio.com/';

admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: dataBaseUrl
});


/*
 * RESTful APIs 
 */
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({
      origin: true
});
const app = express();
app.use(cors);
app.use(cookieParser);

const STATUS_SUCCESS = 'success';
const STATUS_FAIL = 'fail';
const KEY = 'AAAAyECH7aI:APA91bFUahyQwRA5M1UX7bQRU0p7i2QYKfFxmIvg7nhHJINEPaAFg6qX0kK-xsWQJOknIEs2X5FGvgDUzHEGlccYl2q6NXz4nvF28hWh0ZAqLOmdOd9yvSf13AW2Kl2OurrTRdYyNaG7';


app.post('/test', (req, res) => {
      

      // const id = req.body.data.id;
      // const title = req.body.data.title;
      // const startTime = req.body.data.startTime;
      // const instanceToken = req.body.data.instanceToken;

      // console.log('title=' + title + ', instanceToken=' + instanceToken);
      // console.log('id=' + id + ', startTime=' + startTime);

      res.status(200).json({
            status: STATUS_SUCCESS,
      });

      return;
});

app.post('/sendPushNotification', (req, res) => {
      
      const key = req.body.data.key;

      if (key === KEY){

            let title = req.body.data.title;
            if (title === undefined){
                  title = req.query.title;
            }
            const startTime = req.body.data.startTime;
            const instanceToken = req.body.data.instanceToken;
            
            console.log('title=' + title+', startTime='+startTime);
            console.log('token=' + instanceToken);
            console.log('key='+key);

            const message = {

                  notification: {
                        title: title,
                        body: 'Upcoming event tomorrow at '+ startTime
                  },

                  android: {
                        ttl: 5 * 60, //5 min
                        notification: {
                              // sound: 'job_new_offer'
                        },
                  },

                  token: instanceToken,

            };

      console.log('pushNotification  sending...');
      return admin.messaging().send(message)
            .then((response) => {
                  // Response is a message ID string.
                  console.log('pushNotification  Successfully sent message:' + response);

                  res.status(200).json({
                        status: STATUS_SUCCESS,
                  });

                  return ;
            })
            .catch((error) => {
                  console.log('pushNotification Error sending message:' + error);

                  res.status(400).json({
                        status: STATUS_FAIL,
                        msg: error.toString()
                  });

                  return;
            });
      }else{
            //401 Unauthorized (
            res.status(401).json({
                  status: STATUS_FAIL,
                  msg: 'Unauthorized'
            });
      }

});

// Expose the API as a function
exports.api = functions.https.onRequest(app);

// exports.sendEventReminder = functions.database.ref('/events/{pushKey}')
//       .onCreate((snapshot) => {
//             // Grab the current value of what was written to the Realtime Database.

//             const submitted = snapshot.child('isSent').val();
//             if (submitted === true) {
//                   return;
//             }

//             const id = snapshot.child('id').val();
//             const calendarId = snapshot.child('calendarId').val();
//             const title = snapshot.child('title').val();
//             const description = snapshot.child('description').val();
//             const startTime = snapshot.child('startTime').val();
//             const endTime = snapshot.child('endTime').val();
//             const instanceToken = snapshot.child('instanceToken').val();

//             console.log(title + ', '+ description);

//             const message = {

//                   notification: {
//                         title: title,
//                         body: description
//                   },
//                   data: {
//                         id: id.toString(),
//                         calendarId: calendarId.toString(),
//                   },

//                   android: {
//                         ttl: 5 * 60, //5 min
//                         notification: {
//                               sound: 'job_new_offer'
//                         },
//                   },

//                   token: instanceToken,

//             };

//             console.log('pushNotification  sending...');
//             // Send a message to the device corresponding to the provided
//             // registration token.
            
//             return admin.messaging().send(message)
//                   .then((response) => {
//                         // Response is a message ID string.
//                         console.log('pushNotification  Successfully sent message:'+ response);
                        
//                         return snapshot.ref.child('isSent').set(true);
//                   })
//                   .catch((error) => {
//                         console.log('pushNotification Error sending message:'+ error);
//                         return;
//                   });
            

//       });
